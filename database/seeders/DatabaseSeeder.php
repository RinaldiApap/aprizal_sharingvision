<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\Posts::factory(10)->create();

        \App\Models\Posts::factory()->create([
            'title' => 'Ini Alasan Kenapa Kucing Batuk!',
            'content' => 'Ya, kucing bisa mengalami batuk. Meski begitu, frekuensi batuk kucing terjadi tidak sesering seperti pada hewan lainnya. Muntah atau tersedak, termasuk  batuk “hair ball””, sering kali disalah artikan sebagai batuk saluran pernapasan pada kucing. Batuk pada kucing adalah refleks pernapasan yang terjadi ketika saluran pernapasan mereka teriritasi.  Batuk pada kucing bisa menjadi tanda adanya masalah kesehatan, seperti infeksi saluran pernapasan, asma, penyakit jantung, atau kondisi lainnya. Pemahaman mengenai penyebab batuk pada kucing penting agar pemilik kucing dapat mencari perawatan yang tepat jika diperlukan.',
            'category' => 'Hewan',
            'status' => 'Publish'
        ]);
        // 
        \App\Models\Posts::factory()->create([
            'title' => 'Cara Mengatur Jadwal Pola Latihan Gym dalam Seminggu yang Efektif.',
            'content' => 'Olahraga teratur dapat memberikan sejumlah manfaat bagi kesehatan tubuh secara keseluruhan. Apabila Happyfitters sudah terbiasa berolahraga, Happyfitters bisa lho mencoba untuk membuat pola latihan gym dalam seminggu agar lebih efektif. Karena, ternyata banyak banget influencer olahraga Indonesia yang juga punya jadwal sendiri untuk latihan gym. Makanya, gak heran tubuh mereka tampak proporsional. Nah, kalau Happyfitters penasaran bagaimana cara membuat jadwal gym dalam seminggu? Yuk, simak baik-baik ulasan di bawah ini!',
            'category' => 'Olahraga',
            'status' => 'Draft'
        ]);
        // 
        \App\Models\Posts::factory()->create([
            'title' => 'Software Rekomendasi Pembuat Aplikasi Android Terbaik.',
            'content' => 'Di era modernisasi yang canggih teknologi seperti sekarang, siapapun bisa membuat aplikasi yang diimpikan secara mandiri. Hadirnya software pembuat aplikasi android, adalah solusi paling mudah untuk merancang sebuah aplikasi hanya melalui smartphone. Bagi anda yang ingin belajar membuat aplikasi mobile, tentu sangat membutuhkan software khusus untuk membantu proses pembuatan aplikasi tersebut. Beberapa software pengembangan untuk membuat aplikasi android adalah sebagai berikut.',
            'category' => 'Teknologi',
            'status' => 'Thrash'
        ]);
    }
}
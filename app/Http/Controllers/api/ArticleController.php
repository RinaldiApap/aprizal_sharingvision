<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ArticleRequest;
use App\Models\Posts;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    function index(ArticleRequest $request)
    {
        $data = new Posts;
        $data->title = $request->title;
        $data->content = $request->content;
        $data->category = $request->category;
        $data->status = $request->status;
        $data->save();

        if ($data) {
            return Response(['status' => '200', 'message' => 'success', 'data' => $data], 200);
        } else {
            return Response(['status' => '400', 'message' => 'error'], 400);
        }

    }

    function data_article($lmt, $ofs, request $request)
    {
        $offset = $request->has('page') ? $request->get('page') : $ofs;
        $limit = $request->has('limit') ? $request->get('limit') : $lmt;
        $data = Posts::where('status', 0)->limit($limit)->offset(($offset - 1) * $limit)->get()->toArray();

        if ($data) {
            return Response(['status' => '200', 'message' => 'success', 'data' => $data], 200);
        } else {
            return Response(['status' => '400', 'message' => 'error', 'data' => $data], 400);
        }


    }

    function data_article_cat($lmt, $ofs, $stat, request $request)
    {
        $offset = $request->has('page') ? $request->get('page') : $ofs;
        $limit = $request->has('limit') ? $request->get('limit') : $lmt;
        $data = Posts::where('status', $stat)->limit($limit)->offset(($offset - 1) * $limit)->get()->toArray();

        if ($data) {
            return Response(['status' => '200', 'message' => 'success', 'data' => $data], 200);
        } else {
            return Response(['status' => '400', 'message' => 'error', 'data' => $data], 400);
        }
    }
    function data_article_id($id)
    {
        $data = Posts::where('id', $id)->get();
        if ($data) {
            return Response(['status' => '200', 'message' => 'success', 'data' => $data], 200);
        } else {
            return Response(['status' => '400', 'message' => 'error', 'data' => $data], 400);
        }
    }
    function data_article_edit($id, ArticleRequest $request)
    {
        $data = Posts::find($id);
        $data->title = $request->title;
        $data->content = $request->content;
        $data->category = $request->category;
        $data->status = $request->status;
        $data->save();

        if ($data) {
            return Response(['status' => '200', 'message' => 'success', 'data' => $data], 200);
        } else {
            return Response(['status' => '400', 'message' => 'error'], 400);
        }
    }

    function data_article_delete($id)
    {
        $data = Posts::find($id)->delete();
        if ($data) {
            return Response(['status' => '200', 'message' => 'success', 'data' => $data], 200);
        } else {
            return Response(['status' => '400', 'message' => 'error'], 400);
        }
    }

}
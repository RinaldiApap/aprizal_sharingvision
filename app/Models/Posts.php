<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

// use Laravel\Passport\HasApiTokens;

class Posts extends Authenticatable
{
    use HasFactory, Notifiable;
    protected $table = 'posts';
    protected $guarded = ['id'];
}
<?php

use App\Http\Controllers\api\ArticleController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\api\ArticleController as AR;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/test', function () {
    return "OK!";
});

Route::controller(AR::class)->group(
    function () {
        Route::post('article', 'index');
        Route::get('article/{lmt}/{ofs}', 'data_article');
        Route::get('article/{lmt}/{ofs}/{stat}', 'data_article_cat');
        Route::get('article/{id}', 'data_article_id');
        Route::post('article/{id}', 'data_article_edit');
        Route::delete('article/{id}', 'data_article_delete');
    }
);